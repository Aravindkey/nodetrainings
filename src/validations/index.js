const Joi = require('joi');

exports.createEmployee = {
    body: {
        name: Joi.string().max(128).required(),
        address: Joi.string().required(),
        gender: Joi.string().required().valid("male","female"),
        department: Joi.string().required().valid("HR", "Engineering", "Administration"),
        phone: Joi.string().required(),
    },
}

exports.editEmployee = {
    body: {
        name: Joi.string().max(128),
        address: Joi.string(),
        gender: Joi.string().valid("male","female"),
        department: Joi.string().valid("HR", "Engineering", "Administration"),
        phone: Joi.string(),
    },

}
