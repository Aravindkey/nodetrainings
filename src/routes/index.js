const express = require('express');
const employeeRoute = require('./employeeRoute')
const router = express.Router();




router.get('/status', (req, res) => res.send('Status OK'));

router.use('/employees', employeeRoute);

module.exports = router;