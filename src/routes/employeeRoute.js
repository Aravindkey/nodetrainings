const express = require('express');
const validate = require('express-validation');
const { createEmployee } = require('../validations');
const employeeController = require('../controllers/employeeController');

const router = express.Router();

router.get('/dept',employeeController.getEmployeesByDept)
router.get('/:id',employeeController.getEmployeeById)
router.delete('/',employeeController.deleteEmployee)
router.put('/',employeeController.editEmployee);
router.get('/', employeeController.getEmployees);
router.post('/', validate(createEmployee), employeeController.createEmployee)


module.exports = router;