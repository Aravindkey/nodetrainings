const uuid = require('uuid').v4;
const employees = [];

exports.getEmployees = (req, res) => {
    res.send(employees);
};

exports.createEmployee = (req, res) => {
 // console.log(JSON.stringify(req.body));
  const employee = { ...req.body };
  employee.id = uuid();
  employees.push(employee);
  console.log(employees)
  res.json(employee);
};


exports.editEmployee = (req,res) => {

	const employee = { ...req.body }
	let isInEmployee = employees.filter( employees => {
		 if(employees.name === employee.name){
			 employees.phone = employee.phone
			 employees.address = employee.address
			 employees.gender = employee.gender
			 employees.department = employee.department
			 return true
		 } 
		 else
		 	return false
	});
	console.log(isInEmployee)
	res.json(isInEmployee)


};


exports.deleteEmployee = (req,res) => {
	const employee = { ...req.body }
	let isInEmployee = employees.filter( employees => {
		console.log(employee.phone)
		return employee.phone === employees.phone && employees.name === employee.name
	});
	employees.pop(isInEmployee)
	res.json(employees)
};


exports.getEmployeesByDept = (req,res) => {
	const deptToBeGrouped = req.query.dept
	let employee = employees.filter(employees => employees.department === deptToBeGrouped)
	res.json(employee)
}

exports.getEmployeeById = (req,res) => {
	const employeeId = req.params.id
	console.log(employeeId)
	let employeeToBeReturned = employees.filter(employees => {
		return employees.id === employeeId
	})

	res.json(employeeToBeReturned)


};